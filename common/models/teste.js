'use strict';

module.exports = function(Teste) {
    Teste.greet = function(msg, cb) {
      cb(null, 'Greetings... ' + msg);
    }

    Teste.remoteMethod(
        'greet', 
        {
          accepts: {arg: 'msg', type: 'string'},
          returns: {arg: 'greeting', type: 'string'}
        }
    );
};
